import os
import random
import shutil
import pandas as pd

def random_selection(n):
    # Define the source and destination directories
    source_dir = r'C:\Users\David_UoR\OneDrive - University of Reading\CSMPR21_data\CelebA\all_images'
    destination_dir = r'C:\Users\David_UoR\OneDrive - University of Reading\CSMPR21_data\CelebA\selected_images'

    # Create the destination directory if it doesn't exist
    os.makedirs(destination_dir, exist_ok=True)

    # Get a list of all files in the source directory
    all_files = os.listdir(source_dir)

    # Randomly select 10,000 files
    selected_files = random.sample(all_files, n)

    # Copy the selected files to the destination directory
    for file_name in selected_files:
        source_file = os.path.join(source_dir, file_name)
        destination_file = os.path.join(destination_dir, file_name)
        shutil.copy2(source_file, destination_file)

    print(f'Successfully copied {len(selected_files)} files to {destination_dir}')

# randomly select 1700 images 
random_selection(1700)
    
# define the file path of the full image attribute list and images folder
csv_file_path = r'C:\Users\David_UoR\OneDrive - University of Reading\CSMPR21_data\CelebA\list_attr_celeba.csv'
images_directory = r'C:\Users\David_UoR\OneDrive - University of Reading\CSMPR21_data\CelebA\all_images'

# read the CSV file into a DataFrame
df = pd.read_csv(csv_file_path)

# get a list of image files in the directory
image_files = set(os.listdir(images_directory))

# filter the dataframe to keep only rows where the image_id exists in the image_files
df_filtered = df[df['image_id'].isin(image_files)]

# display the first few rows of the filtered DataFrame
print(df_filtered.head())

# initialize a list to store the indices of rows to be deleted
rows_to_delete = []

# iterate over the filtered DataFrame
for index, row in df_filtered.iterrows():
    # check if the Wearing_Hat attribute is 1
    if row['Wearing_Hat'] == 1:
        # define the path to the image file
        image_path = os.path.join(images_directory, row['image_id'])
        # check if the file exists
        if os.path.exists(image_path):
            # Delete the file
            os.remove(image_path)
            # Add the index to the list of rows to delete
            rows_to_delete.append(index)
            print(f"Deleted {image_path}")

# drop the rows from df_filtered
df_filtered = df_filtered.drop(rows_to_delete)

# save the filtered dataframe to a new CSV file
filtered_csv_file_path = r'C:\Users\David_UoR\OneDrive - University of Reading\CSMPR21_data\CelebA\filtered_list_attr_celeba.csv'
df_filtered.to_csv(filtered_csv_file_path, index=False)